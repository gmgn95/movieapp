package com.shimaa.movies.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.bumptech.glide.Glide
import com.shimaa.movies.DetailActivity
import com.shimaa.movies.R
import com.shimaa.movies.model.Movie
import com.shimaa.movies.model.baseImageUrl
import kotlinx.android.synthetic.main.movie_card.view.*

class moviesAdapter(private val context: Context, private val movieList: List<Movie>) : Adapter<moviesAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): moviesAdapter.MyViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.movie_card, viewGroup, false)
        return MyViewHolder(view)

    }

    override fun onBindViewHolder(myViewHolder: MyViewHolder, i: Int) {
        myViewHolder.bind(movieList[i])
    }


    override fun getItemCount(): Int {
        return movieList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movie: Movie) = with(itemView) {
            title.text = movie.title
            Glide.with(context)
                    .load(baseImageUrl+movie.posterPath)
                    .into(image)
            this.setOnClickListener { view ->
                    val intent = Intent(context, DetailActivity::class.java)
                    intent.putExtra("original_title", movie.title)
                    intent.putExtra("poster_path", baseImageUrl+movie.posterPath)
                    intent.putExtra("overview", movie.overview)
                    intent.putExtra("vote_average", movie.voteAverage)
                    intent.putExtra("release_date", movie.releaseDate)
                      intent.putExtra("backdrop", baseImageUrl+ movie.backdropPath)

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
                  //  Toast.makeText(view.context, "You clicked" + movie.getOriginalTitle(), Toast.LENGTH_LONG).show()



            }


        }




    }




}
