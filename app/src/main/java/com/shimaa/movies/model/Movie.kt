package com.shimaa.movies.model

import com.google.gson.annotations.SerializedName
const val baseImageUrl="https://image.tmdb.org/t/p/w500"

class Movie {
    @SerializedName("poster_path")
     val posterPath:String = ""
    @SerializedName("adult")
    val adult:Boolean = false
    @SerializedName("overview")
    val overview:String=""
    @SerializedName("release_date")
    val releaseDate:String=""
    @SerializedName("genre_ids")
    val genreIds:List<Int> = ArrayList()
    @SerializedName("id")
    val id:Int=1
    @SerializedName("original_title")
    val originalTitle:String=""
    @SerializedName("original_language")
    val originalLanguage:String=""
    @SerializedName("title")
    val title:String=""
    @SerializedName("backdrop_path")
    val backdropPath:String=""
    @SerializedName("popularity")
    val popularity:Double=0.0
    @SerializedName("vote_count")
    val voteCount:Int=1
    @SerializedName("video")
    val video:Boolean = false
    @SerializedName("vote_average")
    val voteAverage:Double=0.0

}