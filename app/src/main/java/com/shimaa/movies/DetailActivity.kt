package com.shimaa.movies

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shimaa.movies.R
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    lateinit var nameOfMovie: TextView
    lateinit var plotSynopsis: TextView
    lateinit var userRating: TextView
    lateinit var releaseDate: TextView
    lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        //val toolbar = findViewById<Toolbar>(R.id.toolbar)
      //  setSupportActionBar(toolbar)
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
      //  initCollapsingToolbar()
/*
        imageView = findViewById(R.id.thumbnail_image_header)
        nameOfMovie = findViewById(R.id.title)
        plotSynopsis = findViewById(R.id.plotsynopsis)
        userRating = findViewById(R.id.userrating)
        releaseDate = findViewById(R.id.releasedate)
*/

        initUi()
    }

    private fun initUi() {
        if (intent.hasExtra("original_title")) {
            val movieName = getIntent().extras!!.getString("original_title")
            val synopsis = getIntent().extras!!.getString("overview")
            val rating = getIntent().extras!!.getString("vote_average")
            val dateOfRelease = getIntent().extras!!.getString("release_date")
            val backdropImageUrl = getIntent().extras!!.getString("backdrop")
            val posterPathImageUrl = getIntent().extras!!.getString("poster_path")

            Glide.with(this)
                    .load(backdropImageUrl)
                    .into(backdrop)
            Glide.with(this)
                    .load(posterPathImageUrl)
                    .into(posterimage)
            movie_title.text = movieName
            date_status.text=dateOfRelease

/*
            plotSynopsis.text = synopsis
            userRating.text = rating
            releaseDate.text = dateOfRelease
*/

        } else {
            Toast.makeText(this, "No API Data", Toast.LENGTH_LONG).show()
        }

    }

/*
    private fun initCollapsingToolbar() {
        val collapsingToolbarLayout = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        collapsingToolbarLayout.title = " "
        val appBarLayout = findViewById<AppBarLayout>(R.id.appbar)
        appBarLayout.setExpanded(true)

        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = false
            internal var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = getString(R.string.movie_details)
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
    }
*/
}
